#include "SETriggerRates.h"
#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "CutsSETriggerRates.h"
#include "EventBase.h"
#include "HistSvc.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"
#include <TMath.h>

// Constructor
SETriggerRates::SETriggerRates()
    : Analysis()
{
    // Set up the expected event structure, include branches required for analysis.
    // List the branches required below, see full list here https://luxzeplin.gitlab.io/docs/softwaredocs/analysis/analysiswithrqs/rqlist.html
    m_event->IncludeBranch("eventTPC");
    m_event->IncludeBranch("pulsesTPC");
    m_event->Initialize();
    ////////

    // Setup logging
    logging::set_program_name("SETriggerRates Analysis");

    // Setup the analysis specific cuts.
    m_cutsSETriggerRates = new CutsSETriggerRates(m_event);

    // Setup config service so it can be used to get config variables
    m_conf = ConfigSvc::Instance();
}

// Destructor
SETriggerRates::~SETriggerRates()
{
    delete m_cutsSETriggerRates;
}

/////////////////////////////////////////////////////////////////
// Start of main analysis code block
//
// Initialize() -  Called once before the event loop.
void SETriggerRates::Initialize()
{
    INFO("Initializing SETriggerRates Analysis");
}

// Execute() - Called once per event.
void SETriggerRates::Execute()
{
    float firstTriggerTime = 1637989583; //ER-only 1634511521
    float triggerEndTime   = 140000.;    //ER-only 50400.
    int nTimeBins = triggerEndTime/60;
    float triggerTime = (*m_event->m_eventHeader)->triggerTimeStamp_s-firstTriggerTime;
    
    // Select S2 global triggers
    if ((*m_event->m_eventHeader)->triggerType == 128) {
        m_hists->BookFillHist("Livetime", nTimeBins, 0., triggerEndTime, triggerTime);
        vector<int> pulseTimes = (*m_event->m_tpcPulses)->pulseStartTime_ns;
        vector<float> pulseAreas = (*m_event->m_tpcPulses)->pulseArea_phd;
        
        // pulse closest to zero (trigger index with no cut)
        int zeroIdx = BinarySearch(pulseTimes, 0.);
        // pulse closest to zero with area > 5 phd
        int triggerIdx = FindTriggerPulse(pulseAreas, zeroIdx, 5.);
        
        if (zeroIdx >= 0 && triggerIdx >= 0) {
    
            vector<int> triggerOpts = {zeroIdx, triggerIdx};
            for (int type = 0; type < triggerOpts.size(); type++) {
                int idx = triggerOpts[type];
                string pulseClass = (*m_event->m_tpcPulses)->classification[idx];
                float pulseArea = pulseAreas[idx];
                string areaHist = (type==1) ? "areaLog10_cut" : "areaLog10";
                m_hists->BookFillHist(pulseClass + areaHist, 1000, -1., 7., TMath::Log10(pulseArea));
                string name = (type==1) ? "PulseAreaVsTime_cut" : "PulseAreaVsTime";
                m_hists->BookFillHist(name, nTimeBins, 0., triggerEndTime, 1000, -1., 7., triggerTime, TMath::Log10(pulseArea));

                if (pulseClass == "S2" && pulseArea > 400) { pulseClass = "bigS2";}
                string histName = (type==1) ? "time_cut" : "time";
                m_hists->BookFillHist(pulseClass + histName, nTimeBins, 0., triggerEndTime, triggerTime);
                
    
                float xPos = (*m_event->m_tpcPulses)->topCentroidX_cm[idx];
                float yPos = (*m_event->m_tpcPulses)->topCentroidY_cm[idx];
                string xyHist = (type==1) ? "_xy_centroid_cut" : "_xy_centroid";
                m_hists->BookFillHist(pulseClass + xyHist, 1000, -100., 100., 1000, -100., 100., xPos, yPos);
    
                if (pulseClass=="SE" || pulseClass=="S2" || pulseClass=="bigS2") {
                    float xPos = (*m_event->m_tpcPulses)->s2Xposition_cm[idx];
                    float yPos = (*m_event->m_tpcPulses)->s2Yposition_cm[idx];
                    string xyHist = (type==1) ? "_xy_mercury_cut" : "_xy_mercury";
                    m_hists->BookFillHist(pulseClass + xyHist, 1000, -100., 100., 1000, -100., 100., xPos, yPos);
                }
            
            }
        }
    }
}

// Finalize() - Called once after event loop.
void SETriggerRates::Finalize()
{
    INFO("Finalizing SETriggerRates Analysis");
    m_hists->GetHistFromMap("Livetime")->Scale(4./1000.);
}

int SETriggerRates::BinarySearch(vector<int> const& vec, float value)
{
    int start = 0;
    int end = vec.size() - 1;
    if (vec[0] >= value) return 0;
    if (vec[end] < value) return end;

    while (start <= end) {
        int mid = TMath::Floor((end + start)/2);
        if ((vec[mid-1] <= value) && (vec[mid] >= value)){
            if (TMath::Abs(vec[mid-1]) < TMath::Abs(vec[mid])){
                return mid - 1;
            } else {
                return mid;
            }
        } else {
            if (vec[mid] < value) {
                start = mid + 1;
            } else {
                end = mid - 1;
            }
        }
    }
    return -1;
}

int SETriggerRates::FindTriggerPulse(vector<float> const& areaVec, int idx, float areaCut)
{
    int max = areaVec.size() - idx;
    int cutoff = TMath::Abs(max) - 1;
    for (int delta=0; delta < cutoff; delta++) {
        int iL = idx - delta; int iR = idx + delta;
        float areaL = areaVec[iL]; float areaR = areaVec[iR];
        if (areaL >= areaCut) { return iL; }
        if (areaR >= areaCut) { return iR; }
    }
    return -1;
}
