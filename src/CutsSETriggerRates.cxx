#include "CutsSETriggerRates.h"
#include "ConfigSvc.h"

CutsSETriggerRates::CutsSETriggerRates(EventBase* eventBase)
{
    m_event = eventBase;
}

CutsSETriggerRates::~CutsSETriggerRates()
{
}

// Function that lists all of the common cuts for this Analysis
bool CutsSETriggerRates::SETriggerRatesCutsOK(int pulseID)
{
    return true;
}

// Cut on pulse area (not implemented, may not use...)
bool CutsSETriggerRates::PulseArea(int pulseID)
{
    float area = (*m_event->m_tpcPulses)->pulseArea_phd[pulseID];
    return true; 
}

// Cut pre-trigger window to reduce bias on random triggers
bool CutsSETriggerRates::CutPreTrigger(int pulseID)
{
    if ((*m_event->m_eventHeader)->triggerType == 32) {
        return ((*m_event->m_tpcPulses)->pulseStartTime_ns[pulseID] > 0);
    }
    return true;
}
