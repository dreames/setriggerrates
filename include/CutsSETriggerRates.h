#ifndef CutsSETriggerRates_H
#define CutsSETriggerRates_H

#include "EventBase.h"

class CutsSETriggerRates {

public:
    CutsSETriggerRates(EventBase* eventBase);
    ~CutsSETriggerRates();
    bool SETriggerRatesCutsOK(int pulseID);
    bool PulseArea(int pulseID);
    bool CutPreTrigger(int pulseID);

private:
    EventBase* m_event;
};

#endif
