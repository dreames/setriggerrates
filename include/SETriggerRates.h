#ifndef SETriggerRates_H
#define SETriggerRates_H

#include "Analysis.h"

#include "CutsSETriggerRates.h"
#include "EventBase.h"

#include <TTreeReader.h>
#include <string>

class SkimSvc;

class SETriggerRates : public Analysis {

public:
    SETriggerRates();
    ~SETriggerRates();

    void Initialize();
    void Execute();
    void Finalize();

private:
    int BinarySearch(vector<int> const& vec, float value);
    int FindTriggerPulse(vector<float> const& areaVec, int idx, float areaCut);

protected:
    CutsSETriggerRates* m_cutsSETriggerRates;
    ConfigSvc* m_conf;
};

#endif
